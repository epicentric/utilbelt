export function delay<T = void>(time: number, value?: T)
{
    return new Promise<T>((resolve) => {
        setTimeout(() => resolve(value), time);
    });
}

export function delayReject<T = void>(time: number, value?: any): Promise<T>
{
    return new Promise((resolve, reject) => {
        setTimeout(() => reject(value), time);
    });
}