import {delayReject} from './delay';

export function promiseWithTimeout<T>(promise: Promise<T>, timeoutMs: number, errorString: string)
{
    const timeoutPromise = delayReject<T>(timeoutMs, new Error(errorString));

    return Promise.race([
        promise,
        timeoutPromise
    ]);
}