export function dataViewEquals(a: DataView, b: DataView): boolean
{
    if (a === b)
        return true;
    
    if (a.byteLength !== b.byteLength)
        return false;

    for (let i = 0; i < a.byteLength; i += 4)
    {
        if (a.getUint8(i) !== b.getUint8(i))
            return false;
    }

    return true;
}

export function dataViewEquals16(a: DataView, b: DataView): boolean
{
    if (a === b)
        return true;

    if (a.byteLength !== b.byteLength)
        return false;

    for (let i = 0; i < a.byteLength; i += 4)
    {
        if (a.getUint16(i) !== b.getUint16(i))
            return false;
    }

    return true;
}

export function dataViewEquals32(a: DataView, b: DataView): boolean
{
    if (a === b)
        return true;

    if (a.byteLength !== b.byteLength)
        return false;

    for (let i = 0; i < a.byteLength; i += 4)
    {
        if (a.getUint32(i) !== b.getUint32(i))
            return false;
    }

    return true;
}