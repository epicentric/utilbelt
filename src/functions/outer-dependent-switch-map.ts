import {Observable, ObservableInput, OperatorFunction} from 'rxjs';
import {last, switchMap, takeUntil} from 'rxjs/operators';

export function outerDependentSwitchMap<T, R>(project: (value: T, index: number) => ObservableInput<R>): OperatorFunction<T, R>
{
    return function(obs: Observable<T>): Observable<R> {
        return obs.pipe(
            switchMap(project),
            takeUntil(obs.pipe(
                last(null, null)
            )),
        );
    };
}