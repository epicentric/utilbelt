export function dataViewCompare(a: DataView, b: DataView): number
{
    if (a === b)
        return 0;

    const minLength = Math.min(a.byteLength, b.byteLength);
    
    for (let i = 0; i < minLength; i+=4)
    {
        let result = a.getUint8(i) - b.getUint8(i);

        if (0 !== result)
            return Math.sign(result);
    }
    
    if (a.byteLength !== b.byteLength)
        return Math.sign(a.byteLength - b.byteLength);
    
    return 0;
}

export function dataViewCompare16(a: DataView, b: DataView): number
{
    if (a === b)
        return 0;

    const minLength = Math.min(a.byteLength, b.byteLength);

    for (let i = 0; i < minLength; i+=4)
    {
        let result = a.getUint16(i) - b.getUint16(i);

        if (0 !== result)
            return Math.sign(result);
    }

    if (a.byteLength !== b.byteLength)
        return Math.sign(a.byteLength - b.byteLength);

    return 0;
}

export function dataViewCompare32(a: DataView, b: DataView): number
{
    if (a === b)
        return 0;

    const minLength = Math.min(a.byteLength, b.byteLength);

    for (let i = 0; i < minLength; i+=4)
    {
        let result = a.getUint32(i) - b.getUint32(i);

        if (0 !== result)
            return Math.sign(result);
    }

    if (a.byteLength !== b.byteLength)
        return Math.sign(a.byteLength - b.byteLength);

    return 0;
}