import {Observable} from 'rxjs';
import {throttle} from 'lodash-es';
import {ThrottleSettings} from 'lodash'

export function properThrottle<T>(time: number, settings: ThrottleSettings = { leading: false, trailing: true })
{
    return (source: Observable<T>) => new Observable<T>(observer => {
        const throttleFunc = throttle(v => {
            try
            {
                observer.next(v);
            }
            catch (err)
            {
                observer.error(err);
            }
        }, time, settings);

        return source.subscribe({
            next: throttleFunc,
            error(err)
            {
                throttleFunc.cancel();
                observer.error(err);
            },
            complete()
            {
                throttleFunc.cancel();
                observer.complete();
            }
        });
    });
}