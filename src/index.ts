export * from './objects/deferred';
export * from './functions/delay';
export * from './functions/outer-dependent-switch-map';
export * from './functions/promise-with-timeout';
export * from './functions/proper-exhaust-map';
export * from './functions/proper-throttle';
export * from './functions/data-view-compare';
export * from './functions/data-view-equals';